       Diagnostics d'erreur pour l'�chec de l'analyse/assemblage

  Toutes les restrictions de position ne peuvent �tre satisfaites en m�me temps.
Impossible de satisfaire les restrictions de position suivantes�:


1) Impossible de satisfaire Restriction de premier axe de translation dans la
liaison Connection_9 dans le mod�le MAIN.


2) Impossible de satisfaire Restriction de premier axe de translation dans la
liaison Connection_11 dans le mod�le MAIN.


3) Impossible de satisfaire Restriction de premier axe de translation dans la
liaison Connection_20 dans le mod�le MAIN.


4) Le corps rigide body10 li� par Connection_89 dans le mod�le MAIN ne peut pas
�tre orient� correctement. 


5) Impossible de fermer la boucle cin�matique suivante�: Ground - body10 - body4
- body3 - body8 - Ground


6) Impossible de fermer la boucle cin�matique suivante�: Ground - body10 - body6
- body5 - body9 - Ground


7) Impossible de fermer la boucle cin�matique suivante�: Ground - body7 - body1
- body2 - body10 - Ground


