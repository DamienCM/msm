import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

MD = pd.read_csv('MI.csv')

T = MD['time (s)']

for j in range(MD.shape[1]-2):
    title = MD.columns[j+2].split('\t')[-1][4:]
    print(title)
    plt.plot(T,MD.iloc[:,j+2], label = title)
    
plt.grid()
plt.legend()
plt.show()