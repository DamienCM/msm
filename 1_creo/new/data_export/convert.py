import pandas as pd
import numpy as np
import matplotlib.pyplot as pls


def read_file(path):
    i = 0 # buffer = nbo line
    j = 0 # plot number
    header = 3
    sep = '\t'
    cond = True
    df = {}
    with open(path,'r') as file:
        for _ in range(header):
            line = file.readline()
        T, V = [], []
        while cond :
            i=i+1
            line = file.readline()
            if line == '':
                break
            if line.startswith('plot'):
                if j > 1 :
                    df[plot_name] = V
                    T, V = [], []
                    
                if j == 1: 
                    df['time (s)'] = T
                    j+=1
                    df[plot_name] = V
                    T, V = [], []
                plot_name = line[:-1]

                j+=1
            else :
                t, v = line.split(sep)[:-1]
                T.append(t)
                V.append(v)
    df[plot_name] = V     
    return pd.DataFrame(df)


if __name__=='__main__':
    path = 'MD.grt'
    df = read_file(path)
    print(df.head())
    df.to_csv(path[:-3]+'csv')